#!/bin/sh

apps=(
  zsh
  alacritty
  keepassxc
  insync
  neovim
  nemo
  tmux
  stow
  curl
  clipit
  ttf-hack-nerd
  signal-desktop
  spotify-launcher
  figma-linux-bin
  ripgrep
  nvm
  slack-desktop
  digikam
  darktable
  ranger
  spectacle
  simplescreenrecorder
  nvm
  xclip
  docker
  docker-compose
  awesome-git
  steam
  thunderbird
  gnome-keyring
  protonmail-bridge
  bottom
  kdeconnect-git
  cava
  maim
  betterlockscreen
  fd
)

clean_up=(
  thunar-archive-plugin
  thunar-media-tags-plugin
  thunar-volman
  thunar
)

post_install=(
  nvm install stable
)

######

ssh-keygen -o -a 100 -t ed25519 -f ~/.ssh/id_ed25519
cat ~/.ssh/id_ed25519.pub | xclip -selection clipboard
echo "The ssh key has been copied to the clipboard, please add to github and gitlab accounts..."
read -p "When completed, please press enter to continue"

###

mkdir -p ~/Repostitories/{gitlab,github}/NigelGreenway
echo "Created repository directories"

###

git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm

echo "Fetching config"
cd ~/Repositories/gitlab/NigelGreenway/

git clone --recurse-submodules git@gitlab.com:NigelGreenway/config.git ~/Repostitories/gitlab/NigelGreenway/config-git

curl -sL https://github.com/Angelmmiguel/pm/archive/refs/heads/master.tar.gz | tar xz
mv ./pm-master/** ~/.config/zsh/custom/plugins/pm
rm -rf ./pm-master

echo "Running Config"
cd ~/Repositories/gitlab/NigelGreenway/config-main/
sh ./main install

echo "Creating configuration"
yay -Sua --save --answerclean All --answerdiff None --removemake --editor vi --cleanafter true --noconfirm

echo "Installing apps"
for app in ${apps[@]}
do
  echo y | yay -S "$app"
done

sudo groupadd -f docker
sudo usermod -aG docker $USER
newgrp docker
sudo systemctl enable docker.service
sudo systemctl enable containerd.service

docker info

read -p "`docker info` should now show a successful output without errors..."

echo "Install clean up"
yay -Rdnss ${clean_up[@]}

nvm install stable

# Add KDE Connect to the firefwall
sudo firewall-cmd --permanent --zone=public --add-service=kdeconnect
sudo firewall-cmd --reload

sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"


